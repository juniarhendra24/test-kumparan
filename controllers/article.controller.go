package controllers

import (
	"encoding/json"
	"kumparan/models"
	"net/http"

	"github.com/labstack/echo/v4"
)

func FetchAllArticle(c echo.Context) error {
	filter := c.QueryParam("author")
	search := c.QueryParam("search")

	result, err := models.FetchAllArticle(filter, search)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)
}

func StoreArticle(c echo.Context) error {
	art := models.Article{}

	defer c.Request().Body.Close()
	json.NewDecoder(c.Request().Body).Decode(&art)
	result, err := models.StoreArticle(art.Author, art.Title, art.Body)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, result)
}
