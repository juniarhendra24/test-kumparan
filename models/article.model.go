package models

import (
	"context"
	"fmt"
	"kumparan/db"
	"net/http"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

type Article struct {
	Id      int    `json:"id"`
	Author  string `json:"author"`
	Title   string `json:"title"`
	Body    string `json:"body"`
	Created string `json:"created"`
}

type Condition struct {
	Filter string `json:"filter"`
}

func FetchAllArticle(filter string, search string) (Response, error) {
	var obj Article
	var arrobj []Article
	var res Response

	//mysql
	con := db.CreateCon()

	sqlStatement := "SELECT * FROM article WHERE 1=1"

	if filter != "" {
		sqlStatement = fmt.Sprintf("%s AND author = '%s'", sqlStatement, filter)
	}

	if search != "" {
		sqlStatement = fmt.Sprintf("%s AND title LIKE '%%%s%%' OR body LIKE '%%%s%%'", sqlStatement, search, search)
	}

	sqlStatement = fmt.Sprintf("%s ORDER BY created DESC", sqlStatement)

	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {
		err = rows.Scan(&obj.Id, &obj.Author, &obj.Title, &obj.Body, &obj.Created)
		if err != nil {
			return res, err
		}

		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = arrobj

	return res, nil
}

func StoreArticle(author string, title string, body string) (Response, error) {
	var res Response

	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	rdb.Set(ctx, author+"___"+title, body, 0).Err()

	//mysql
	con := db.CreateCon()

	sqlStatement := "INSERT article (author, title, body) VALUES (?, ?, ?)"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(author, title, body)
	if err != nil {
		return res, err
	}

	lastInsertedId, err := result.LastInsertId()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"last_inserted_id": lastInsertedId,
	}

	return res, nil
}
