package routes

import (
	"kumparan/controllers"

	"net/http"

	"github.com/labstack/echo/v4"
)

func Init() *echo.Echo {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Kumparan Skill Test")
	})

	e.GET("/article", controllers.FetchAllArticle)
	e.POST("/article", controllers.StoreArticle)
	return e
}
